import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import { Box } from '@material-ui/core';

import Navbar from './components/Navbar';
import Videos from './containers/Videos';
import Categories from './containers/Categories';
import Equipments from './containers/Equipments';
import Exercises from './containers/Exercises';
import Groups from './containers/Groups';
import Recipes from './containers/Recipes';
import Blogs from './containers/Blog';
import Payments from './containers/Payments/Payments';
import Users from './containers/Users';
import Coupons from './containers/Coupons';

function Home() {
  return (
    <>
      <h2>Főoldal</h2>
      <Box>Verziószám: 1.5.0</Box>

      <br />
      <Box>Szöveg formázási segédlet</Box>
      <Box style={{ fontWeight: 'bold' }}>{'<b>Félkövér</b>'}</Box>
      <Box style={{ fontStyle: 'italic' }}>{'<i>Dőlt</i>'}</Box>
      <Box style={{ textDecoration: 'underline' }}>{'<u>Aláhúzott</u>'}</Box>
    </>
  );
}

const App: React.FC = () => (
  <Router>
    <Navbar />
    <div style={{ padding: '2rem' }}>
      <Switch>
        <Route path="/videos">
          <Videos />
        </Route>
        <Route path="/users">
          <Users />
        </Route>
        <Route path="/recipes">
          <Recipes />
        </Route>
        <Route path="/blogs">
          <Blogs />
        </Route>
        <Route path="/categories">
          <Categories />
        </Route>
        <Route path="/equipments">
          <Equipments />
        </Route>
        <Route path="/exercises">
          <Exercises />
        </Route>
        <Route path="/groups">
          <Groups />
        </Route>
        <Route path="/payments">
          <Payments />
        </Route>
        <Route path="/coupons">
          <Coupons />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </div>
  </Router>
);

export default App;
