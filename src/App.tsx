import React from 'react';
import { hot } from 'react-hot-loader';
import { ThemeProvider } from '@material-ui/core/styles';
import Routes from './Routes';
// TODO: Fix absolute path errors on compile
import theme from './utils/theme';

const App: React.FC = () => (
  <ThemeProvider theme={theme}>
    <Routes />
  </ThemeProvider>
);

export default hot(module)(App);
