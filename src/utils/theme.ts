import { createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
// https://preview.keenthemes.com/metronic/demo13/index.html

export default createMuiTheme({
  palette: {
    primary: blue,
  },
  overrides: {
    MuiPaper: {
      root: {
        color: '#3f4254',
      },
      elevation1: {
        boxShadow: '0 10px 30px 0 rgba(82,63,105,.08)',
      },
    },
    // Style sheet name ⚛️
    MuiTab: {
      root: {
        textTransform: 'capitalize',
        fontWeight: 600,
      },
      textColorPrimary: {
        '&:hover': {
          color: '#3699ff',
        },
        color: '#7e8299',
        '&$selected': {
          color: '#3699ff',
          background: '#f3f6f9',
        },
      },
    },
    MuiCardContent: {
      root: {
        padding: 22,
      },
    },
    MuiCardActions: {
      root: {
        paddingLeft: 22,
      },
    },
    MuiCardHeader: {
      root: {
        padding: 22,
        margin: 'unset',
      },
    },
    MuiOutlinedInput: {
      root: {
        borderRadius: 6,
      },
      input: {
        padding: '8.5px 12px',
      },
    },
  },
});
