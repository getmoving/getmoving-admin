import { fromImage } from 'imtool';

const resizeFile = async (file: File): Promise<Blob> => {
  const tool = await fromImage(file);
  return tool.thumbnail(400).toBlob();
};

export default resizeFile;
