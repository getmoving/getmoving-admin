import React from 'react';
import { Grid, Box } from '@material-ui/core';

import useBlogs from './useBlogs';
import BlogForm from './components/BlogForm';
import CategorySelect from './components/CategorySelect';
import CategoriesList from './components/CategoriesList';
import BlogList from './components/BlogList';

const Blogs: React.FC = () => {
  const {
    dispatch, state, handleClick, isLoading, data, categories, categoryUpload, category,
    setCategory,
  } = useBlogs();

  return (
    <Box>
      <Grid container spacing={4}>
        <Grid item xs={12} md={8}>
          <BlogForm
            state={state}
            dispatch={dispatch}
            handleClick={handleClick}
            isLoading={isLoading}
            categories={categories}
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <CategorySelect
            handleClick={categoryUpload}
            category={category}
            setCategory={setCategory}
            isLoading={isLoading}
          />
          <Box m={2} />
          <CategoriesList data={categories} />
        </Grid>
      </Grid>
      <Box m={4} />
      <Grid item xs={12} md={12} lg={12}>
        <BlogList data={data} />
      </Grid>
    </Box>
  );
};

export default Blogs;
