export const blogInitialState = {
  title: '',
  details: '',
  body1: '',
  body2: '',
  coverFile: null as (File | null),
  imageFile1: null as (File | null),
  imageFile2: null as (File | null),
  categoryIds: [] as string[],
};

export type BlogState = typeof blogInitialState;
type Action = Partial<BlogState>;

interface Props extends BlogState {
  id: string, image1Url: string, image2Url: string, coverUrl: string,
}
export type BlogProps = Omit<Props, 'imageFile1' | 'imageFile2' | 'coverFile'>

export const blogReducer = (s: BlogState, a: Action) => ({ ...s, ...a });
