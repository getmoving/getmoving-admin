import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent, CardActions, Button, CircularProgress } from '@material-ui/core';

import NameInput from './NameInput';

interface Props {
  category: string;
  isLoading: boolean;
  handleClick: () => void;
  setCategory: (v: string) => void;
}

const CategoryForm: React.FC<Props> = ({
  handleClick, category, setCategory, isLoading,
}) => (
  <Card>
    <CardHeader title="Kategória feltöltése" component="h3" disableTypography />
    <Divider />
    <CardContent>
      <form>
        <Grid container spacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <NameInput
              name={category}
              handleSet={setCategory}
            />
          </Grid>
        </Grid>
      </form>
    </CardContent>
    <Divider />
    <CardActions>
      <Button
        endIcon={isLoading && <CircularProgress size={16} />}
        disabled={isLoading || !category}
        variant="contained"
        type="submit"
        color="primary"
        onClick={handleClick}
      >
        Feltöltés
      </Button>
    </CardActions>
  </Card>
);

export default CategoryForm;
