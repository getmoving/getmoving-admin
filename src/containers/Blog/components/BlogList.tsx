import React from 'react';
import { Table, TableBody, TableRow, TableCell, TableHead, Divider, CardHeader, Card, CardContent } from '@material-ui/core';
import { BlogProps } from '../Blog.reducer';

const BlogList: React.FC<{ data: BlogProps[] }> = ({ data }) => {
  const renderBody = () => data.map((b) => (
    <TableRow key={b.id}>
      <TableCell>{b.title}</TableCell>
      <TableCell>{b.details}</TableCell>
      <TableCell>
        <img
          alt="blog"
          src={b.coverUrl}
          style={{ maxWidth: 200, height: 100, objectFit: 'contain' }}
        />
      </TableCell>
    </TableRow>
  ));

  return (
    <Card>
      <CardHeader title="Blog lista" component="h3" disableTypography />
      <Divider />
      <CardContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Név</TableCell>
              <TableCell>Leírás</TableCell>
              <TableCell>Kép</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderBody()}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};

export default BlogList;
