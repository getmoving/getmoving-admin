import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent, CardActions, Button, CircularProgress, Typography } from '@material-ui/core';

import DropZone from '../../../components/DropZone';
import NameInput from './NameInput';
import { BlogState } from '../Blog.reducer';
import DetailsInput from './DetailsInput';
import Body1Input from './Body1Input';
import Body2Input from './Body2Input';
import { BlogCategory } from '../Blog.types';
import CategoryInput from './CategoryInput';

interface Props {
  isLoading: boolean;
  handleClick: () => void;
  state: BlogState;
  dispatch: React.Dispatch<Partial<BlogState>>
  categories: BlogCategory[];
}

const BlogForm: React.FC<Props> = ({ handleClick, state, dispatch, isLoading, categories }) => (
  <Card>
    <CardHeader title="Blog feltöltése" component="h3" disableTypography />
    <Divider />
    <CardContent>
      <form>
        <Grid container spacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <NameInput
              name={state.title}
              handleSet={(val) => dispatch({ title: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <DetailsInput
              val={state.details}
              handleSet={(val) => dispatch({ details: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <CategoryInput
              data={categories}
              val={state.categoryIds}
              handleSet={(v) => dispatch({ categoryIds: v })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <Body1Input
              val={state.body1}
              handleSet={(val) => dispatch({ body1: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <Body2Input
              val={state.body2}
              handleSet={(val) => dispatch({ body2: val })}
            />
          </Grid>
          <Grid item xs={2} md={2} lg={2}>
            <Typography>Borító kép</Typography>
          </Grid>
          <Grid item xs={10} md={10} lg={10}>
            <DropZone file={state.coverFile} setFile={(f) => dispatch({ coverFile: f })} />
          </Grid>
          <Grid item xs={2} md={2} lg={2}>
            <Typography>1. Kép (opcionális)</Typography>
          </Grid>
          <Grid item xs={10} md={10} lg={10}>
            <DropZone square file={state.imageFile1} setFile={(f) => dispatch({ imageFile1: f })} />
          </Grid>
          <Grid item xs={2} md={2} lg={2}>
            <Typography>2. Kép (opcionális)</Typography>
          </Grid>
          <Grid item xs={10} md={10} lg={10}>
            <DropZone square file={state.imageFile2} setFile={(f) => dispatch({ imageFile2: f })} />
          </Grid>
        </Grid>
      </form>
    </CardContent>
    <Divider />
    <CardActions>
      <Button
        endIcon={isLoading && <CircularProgress size={16} />}
        disabled={isLoading || !state.title || !state.body1 || !state.body2
            || !state.details || !state.coverFile || !state.categoryIds.length}
        variant="contained"
        type="submit"
        color="primary"
        onClick={handleClick}
      >
          Feltöltés
      </Button>
    </CardActions>
  </Card>
);

export default BlogForm;
