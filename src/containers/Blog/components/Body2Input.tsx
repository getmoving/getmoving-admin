import { Grid, Icon, InputAdornment, TextField, Tooltip, Typography } from '@material-ui/core';
import React from 'react';
import InfoIcon from '@material-ui/icons/Info';

interface Props {
  val: string;
  handleSet: React.Dispatch<string>;
}

const Body2Input: React.FC<Props> = ({ val, handleSet }) => (
  <Grid container alignItems="center">
    <Grid item xs={2} md={2} lg={2}>
      <Typography>Szövegtörzs 2</Typography>
    </Grid>
    <Grid item xs={10} md={10} lg={10}>
      <TextField
        variant="outlined"
        style={{ width: '100%' }}
        id="body"
        value={val}
        multiline
        onChange={(e) => handleSet(e.target.value)}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <Tooltip title="A képek utáni blog bejegyzés">
                <Icon color="primary">
                  <InfoIcon />
                </Icon>
              </Tooltip>
            </InputAdornment>
          ),
        }}
      />
    </Grid>
  </Grid>
);

export default Body2Input;
