import React, { useReducer } from 'react';
import axios from 'axios';
import useAxios from 'axios-hooks';

import { storage } from '../../utils/firebase';
import { API_ROOT } from '../../utils/constants';
import uuid from '../../utils/uuid';
import resizeFile from '../../utils/resizeFile';
import { blogReducer, blogInitialState, BlogProps } from './Blog.reducer';
import { BlogCategory } from './Blog.types';

const useBlogs = () => {
  const [state, dispatch] = useReducer(blogReducer, blogInitialState);
  const [{ data = [] }, blogsRefetch] = useAxios<BlogProps[]>(`${API_ROOT}/admin/get-blogs`);
  const [{ data: categories = [] }, categoryRefetch] = useAxios<BlogCategory[]>(`${API_ROOT}/admin/get-blog-categories`);
  const [isLoading, setLoading] = React.useState(false);
  const [category, setCategory] = React.useState('');

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { imageFile1, imageFile2, coverFile, ...rest } = state;

  const handleClick = async () => {
    if (!state.coverFile) return;
    const thumbnail = await resizeFile(state.coverFile);

    setLoading(true);
    const uploadedCover = await storage.ref().child(`blogs/${uuid()}`).put(state.coverFile);
    const uploadedThumbnail = await storage.ref().child(`blogs/${uuid()}`).put(thumbnail);
    const coverUrl = await uploadedCover.ref.getDownloadURL();
    const thumbnailUrl = await uploadedThumbnail.ref.getDownloadURL();

    let image1Url;
    let image2Url;
    if (state.imageFile1) {
      const uploadedImage1 = await storage.ref().child(`blogs/${uuid()}`).put(state.imageFile1);
      image1Url = await uploadedImage1.ref.getDownloadURL() as string;
    }
    if (state.imageFile2) {
      const uploadedImage2 = await storage.ref().child(`blogs/${uuid()}`).put(state.imageFile2);
      image2Url = await uploadedImage2.ref.getDownloadURL() as string;
    }

    if (!coverUrl || !thumbnailUrl) return;
    try {
      await axios.post(`${API_ROOT}/admin/upload-blog`, { ...rest,
        coverUrl,
        thumbnailUrl,
        image1Url,
        image2Url });
      await blogsRefetch();
      dispatch(blogInitialState);
      // eslint-disable-next-line no-alert
      alert('Blog hozzáadva!');
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(`Hiba: ${error.message}`);
    } finally {
      setLoading(false);
    }
  };

  const categoryUpload = async () => {
    if (!category) return;
    setLoading(true);
    try {
      await axios.post(`${API_ROOT}/admin/upload-blog-category`, { name: category });
      await categoryRefetch();
      setCategory('');
      // eslint-disable-next-line no-alert
      alert('Kategória hozzáadva!');
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(`Hiba: ${error.message}`);
    } finally {
      setLoading(false);
    }
  };

  return {
    dispatch,
    state,
    handleClick,
    isLoading,
    data,
    categoryRefetch,
    categories,
    category,
    setCategory,
    categoryUpload,
  };
};

export default useBlogs;
