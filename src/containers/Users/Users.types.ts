export interface UsersProps {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  address?: {
    city: string;
    number: string;
    street: string;
    zipCode: string;
  }
  payment?: {
    isActive: boolean;
    isCancelled: boolean;
    paymentType: string;
    validUntil: number;
  }
}
