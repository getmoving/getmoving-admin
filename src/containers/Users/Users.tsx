import React from 'react';
import { Grid, CircularProgress } from '@material-ui/core';

import List from './components/UsersList';
import useUsers from './useUsers';

const Users: React.FC = () => {
  const { data, isLoading } = useUsers();

  if (isLoading) return (<CircularProgress />);

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} md={12} lg={12}>
        <List data={data} />
      </Grid>
    </Grid>
  );
};

export default Users;
