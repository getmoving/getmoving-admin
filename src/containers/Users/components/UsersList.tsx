import React from 'react';
import { Table, TableBody, TableRow, TableCell, TableHead, Divider, CardHeader, Card, CardContent } from '@material-ui/core';
import { UsersProps } from '../Users.types';

function sortByDate(a: UsersProps, b: UsersProps) {
  if (a.payment && !b.payment) return -1;
  if (!a.payment && b.payment) return 1;

  if (b.payment && a.payment && (Math.abs(new Date(b.payment.validUntil).getTime())
  > Math.abs(new Date(a.payment.validUntil).getTime()))) {
    return 1;
  }

  if (b.payment && a.payment && Math.abs(new Date(b.payment.validUntil).getTime())
  < Math.abs(new Date(a.payment.validUntil).getTime())) {
    return -1;
  }
  return 0;
}

const UsersList: React.FC<{ data: UsersProps[] }> = ({ data }) => {
  const renderBody = () => data.sort(sortByDate).map((e) => (
    <TableRow key={e.id}>
      <TableCell>{e.email}</TableCell>
      <TableCell>{`${e.lastName} ${e.firstName}`}</TableCell>
      {e.address ? (
        <TableCell>
          {`${e.address.zipCode
          } ${e.address.city} ${e.address.street} ${e.address.number}`}
        </TableCell>
      ) : <TableCell>-</TableCell>}
      {e.payment ? <TableCell>{e.payment.isActive ? 'Aktív' : '-'}</TableCell> : <TableCell>-</TableCell>}
      {e.payment ? <TableCell>{e.payment.isCancelled ? 'Lemondva' : '-'}</TableCell> : <TableCell>-</TableCell>}
      {e.payment
        ? <TableCell>{new Date(e.payment.validUntil).toLocaleDateString()}</TableCell>
        : <TableCell>-</TableCell>}
      {e.payment ? <TableCell>{e.payment.paymentType}</TableCell> : <TableCell>-</TableCell>}
    </TableRow>
  ));

  return (
    <Card>
      <CardHeader title="Felhasználók" component="h3" disableTypography />
      <Divider />
      <CardContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Email</TableCell>
              <TableCell>Név</TableCell>
              <TableCell>Cím</TableCell>
              <TableCell>Aktív</TableCell>
              <TableCell>Lemondva</TableCell>
              <TableCell>Érvényes</TableCell>
              <TableCell>Típus</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderBody()}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};

export default UsersList;
