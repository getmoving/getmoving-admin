import useAxios from 'axios-hooks';

import { API_ROOT } from '../../utils/constants';
import { UsersProps } from './Users.types';

const useUsers = () => {
  const [{ data = [], loading }] = useAxios<UsersProps[]>(`${API_ROOT}/admin/get-users`);

  return { isLoading: loading, data };
};

export default useUsers;
