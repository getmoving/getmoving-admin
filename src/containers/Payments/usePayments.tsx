import useAxios from 'axios-hooks';

import { API_ROOT } from '../../utils/constants';
import { PaymentsProps } from './Payments.types';

const usePayments = () => {
  const [{ data = [], loading }] = useAxios<PaymentsProps[]>(`${API_ROOT}/admin/get-payments`);

  return { isLoading: loading, data };
};

export default usePayments;
