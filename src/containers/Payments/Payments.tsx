import React from 'react';
import { Grid, CircularProgress } from '@material-ui/core';

import List from './components/PaymentsList';
import usePayments from './usePayments';

const Payments: React.FC = () => {
  const { data, isLoading } = usePayments();

  if (isLoading) return (<CircularProgress />);

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} md={12} lg={12}>
        <List data={data} />
      </Grid>
    </Grid>
  );
};

export default Payments;
