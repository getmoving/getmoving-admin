import React from 'react';
import { Table, TableBody, TableRow, TableCell, TableHead, Divider, CardHeader, Card, CardContent } from '@material-ui/core';
import { PaymentsProps } from '../Payments.types';

const PaymentsList: React.FC<{ data: PaymentsProps[] }> = ({ data }) => {
  const renderBody = () => data
    .sort((a, b) => Math.abs(new Date(b.CompletedAt).getTime() - new Date(a.CompletedAt).getTime()))
    .map((e) => (
      <TableRow key={e.id}>
        <TableCell>{e.id}</TableCell>
        <TableCell>{e.Status}</TableCell>
        <TableCell>{e.Total}</TableCell>
        <TableCell>{new Date(e.CompletedAt).toLocaleDateString()}</TableCell>
        <TableCell>
          {e.Transactions && e.Transactions[0].Payer && e.Transactions[0].Payer.Email}
        </TableCell>
      </TableRow>
    ));

  return (
    <Card>
      <CardHeader title="Fizetések" component="h3" disableTypography />
      <Divider />
      <CardContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Státusz</TableCell>
              <TableCell>Összeg</TableCell>
              <TableCell>Dátum</TableCell>
              <TableCell>Email</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderBody()}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};

export default PaymentsList;
