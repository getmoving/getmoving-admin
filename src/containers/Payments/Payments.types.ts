export interface PaymentsProps {
  id: string;
  userId: string;
  CompletedAt: string;
  Status: string;
  Total: number;
  Transactions: {
    Payer: {
      Email: string;
    }
  }[]
}
