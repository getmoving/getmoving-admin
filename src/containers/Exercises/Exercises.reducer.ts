import { COUNTING_TYPE } from './Exercises.types';

export const exercisesInitialState = {
  name: '',
  videoId: '',
  categoryIds: [] as string[],
  count: '' as unknown as number,
  countingType: COUNTING_TYPE.REP,
  equipmentIds: [] as string[],
};

type State = typeof exercisesInitialState;
type Action = Partial<State>;

export type ExercisesProps = State;
export const exercisesReducer = (s: State, a: Action) => ({ ...s, ...a });
