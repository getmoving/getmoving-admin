import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent, CardActions, Button, CircularProgress } from '@material-ui/core';

import { COUNTING_TYPE } from '../Exercises.types';
import VideoSelect from './VideoSelect';
import CategorySelect from './CategorySelect';
import CountingTypeSelect from './CountingTypeSelect';
import CountingInput from './CountingInput';
import EquipmentsSelect from './EquipmentsSelect';
import { ExercisesProps } from '../Exercises.reducer';

interface Props {
  handleClick: () => void;
  videoId: string;
  categoryIds: string[];
  countingType: COUNTING_TYPE;
  count: number;
  equipmentIds: string[];
  isLoading: boolean;
  data: ExercisesProps[];
  dispatch: React.Dispatch<Partial<{
    name: string;
    videoId: string;
    categoryIds: string[];
    countingType: COUNTING_TYPE;
    count: number;
    equipmentIds: string[];
  }>>
}

const ExercisesForm: React.FC<Props> = ({
  handleClick, dispatch, isLoading, videoId, categoryIds, countingType, count, equipmentIds, data,
}) => (
  <Card>
    <CardHeader title="Gyakorlat feltöltése" component="h3" disableTypography />
    <Divider />
    <CardContent>
      <form>
        <Grid container spacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <VideoSelect
              val={videoId}
              handleSet={(val) => dispatch({ videoId: val })}
              setName={(v) => dispatch({ name: v })}
              exercises={data}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <CategorySelect
              val={categoryIds}
              handleSet={(val) => dispatch({ categoryIds: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <EquipmentsSelect
              val={equipmentIds}
              handleSet={(val) => dispatch({ equipmentIds: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <CountingTypeSelect
              val={countingType}
              handleSet={(val) => dispatch({ countingType: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <CountingInput
              val={count}
              handleSet={(val) => dispatch({ count: val })}
              countingType={countingType}
            />
          </Grid>
        </Grid>
      </form>
    </CardContent>
    <Divider />
    <CardActions>
      <Button
        endIcon={isLoading && <CircularProgress size={16} />}
        disabled={isLoading || !videoId || !categoryIds.length || !countingType || !count}
        variant="contained"
        type="submit"
        color="primary"
        onClick={handleClick}
      >
        Feltöltés
      </Button>
    </CardActions>
  </Card>
);

export default ExercisesForm;
