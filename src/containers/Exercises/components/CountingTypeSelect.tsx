import React from 'react';
import { Grid, Select, FormControl, InputLabel, MenuItem } from '@material-ui/core';
import { COUNTING_TYPE } from '../Exercises.types';

interface Props {
  val: COUNTING_TYPE;
  handleSet: React.Dispatch<COUNTING_TYPE>;
}

const CountingTypeSelect: React.FC<Props> = ({ val, handleSet }) => (
  <Grid container alignItems="center">
    <Grid item xs={12} md={12} lg={12}>
      <FormControl style={{ width: '100%' }}>
        <InputLabel shrink>Válassz számlálás típust</InputLabel>
        <Select
          value={val}
          onChange={(e) => handleSet(e.target.value as COUNTING_TYPE)}
        >
          <MenuItem value={COUNTING_TYPE.REP}>Ismétlésszám</MenuItem>
          <MenuItem value={COUNTING_TYPE.DUR}>Időtartam</MenuItem>
        </Select>
      </FormControl>
    </Grid>
  </Grid>
);

export default CountingTypeSelect;
