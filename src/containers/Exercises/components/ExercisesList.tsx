import React from 'react';
import { Table, TableBody, TableRow, TableCell, TableHead, Divider, CardHeader, Card, CardContent } from '@material-ui/core';
import useAxios from 'axios-hooks';

import { CategoriesProps } from 'src/containers/Categories/Categories.types';
import { EquipmentsProps } from 'src/containers/Equipments/Equipments.types';
import { VideosProps } from 'src/containers/Videos/Videos.types';
import { API_ROOT } from '../../../utils/constants';
import { ExercisesProps } from '../Exercises.reducer';
import { COUNTING_TYPE } from '../Exercises.types';

const ExercisesList: React.FC<{ data: ExercisesProps[] }> = ({ data }) => {
  const [{ data: categories = [] }] = useAxios<CategoriesProps[]>(`${API_ROOT}/admin/get-categories`);
  const [{ data: equipments = [] }] = useAxios<EquipmentsProps[]>(`${API_ROOT}/admin/get-equipments`);
  const [{ data: videos = [] }] = useAxios<VideosProps[]>(`${API_ROOT}/admin/get-videos`);

  const renderBody = () => data.map((e) => {
    const video = videos.find((v) => v.id === e.videoId);

    return (
      <TableRow key={e.videoId}>
        <TableCell>{video ? video.title : '-'}</TableCell>
        <TableCell>{e.categoryIds.map((id) => {
          const category = categories.find((e) => e.id === id);
          return `${category ? category.name : '-'} `;
        })}
        </TableCell>
        <TableCell>{e.countingType === COUNTING_TYPE.REP ? 'Ismétlésszám' : 'Másodperc'}</TableCell>
        <TableCell>{e.count}</TableCell>
        <TableCell>{e.equipmentIds.map((id) => {
          const equipment = equipments.find((e) => e.id === id);
          return `${equipment ? equipment.name : '-'} `;
        })}
        </TableCell>
      </TableRow>
    );
  });

  return (
    <Card>
      <CardHeader title="Gyakorlat lista" component="h3" disableTypography />
      <Divider />
      <CardContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Video neve</TableCell>
              <TableCell>Kategória</TableCell>
              <TableCell>Számlálás típus</TableCell>
              <TableCell>Ismétlés</TableCell>
              <TableCell>Felszerelések</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderBody()}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};

export default ExercisesList;
