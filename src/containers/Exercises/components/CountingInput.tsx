import { Grid, TextField, Typography } from '@material-ui/core';
import React from 'react';
import { COUNTING_TYPE } from '../Exercises.types';

interface Props {
  val: number;
  handleSet: React.Dispatch<number>;
  countingType: COUNTING_TYPE;
}

const CountingInput: React.FC<Props> = ({ countingType, val, handleSet }) => (
  <Grid container alignItems="center">
    <Grid item xs={4} md={4} lg={4}>
      <Typography>{countingType === COUNTING_TYPE.REP ? 'Ismétlésszám' : 'Másodperc'}</Typography>
    </Grid>
    <Grid item xs={8} md={8} lg={8}>
      <TextField
        variant="outlined"
        type="number"
        style={{ width: '100%' }}
        id="count"
        value={val}
        onChange={(e) => handleSet(e.target.value ? Number(e.target.value) : '' as unknown as number)}
      />
    </Grid>
  </Grid>
);

export default CountingInput;
