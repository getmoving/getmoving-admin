import React from 'react';
import { Grid, Select, FormControl, InputLabel, MenuItem, Input, Chip } from '@material-ui/core';
import useAxios from 'axios-hooks';

import { EquipmentsProps } from 'src/containers/Equipments/Equipments.types';
import { API_ROOT } from '../../../utils/constants';

interface Props {
  val: string[];
  handleSet: React.Dispatch<string[]>;
}

const EquipmentsSelect: React.FC<Props> = ({ val, handleSet }) => {
  const [{ data = [] }] = useAxios<EquipmentsProps[]>(`${API_ROOT}/admin/get-equipments`);

  return (
    <Grid container alignItems="center">
      <Grid item xs={12} md={12} lg={12}>
        <FormControl style={{ width: '100%' }}>
          <InputLabel shrink>Válassz felszereléseket</InputLabel>
          <Select
            multiple
            value={val}
            onChange={(e) => handleSet(e.target.value as string[])}
            input={<Input />}
            renderValue={(selected) => (
              <div>
                {(selected as string[]).map((value) => {
                  const selectedData = data.find((d) => d.id === value);
                  return selectedData && <Chip key={value} label={selectedData.name} />;
                })}
              </div>
            )}
          >
            {data.map((e) => (
              <MenuItem key={e.id} value={e.id}>
                {e.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
    </Grid>
  );
};

export default EquipmentsSelect;
