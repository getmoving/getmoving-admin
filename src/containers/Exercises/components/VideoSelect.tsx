import React from 'react';
import { Grid, Select, FormControl, InputLabel, MenuItem } from '@material-ui/core';
import useAxios from 'axios-hooks';

import { API_ROOT } from '../../../utils/constants';
import { VideosProps } from '../../Videos/Videos.types';
import { ExercisesProps } from '../Exercises.reducer';

interface Props {
  val: string;
  handleSet: React.Dispatch<string>;
  exercises: ExercisesProps[];
  setName: (name: string) => void;
}

const VideoSelect: React.FC<Props> = ({ val, handleSet, exercises, setName }) => {
  const [{ data = [] }] = useAxios<VideosProps[]>(`${API_ROOT}/admin/get-videos`);
  const checkIfAlreadyAdded = (id: string) => !!exercises.find((e) => e.videoId === id);

  return (
    <Grid container alignItems="center">
      <Grid item xs={12} md={12} lg={12}>
        <FormControl style={{ width: '100%' }}>
          <InputLabel shrink>Válassz egy videót</InputLabel>
          <Select
            value={val}
            onChange={(e) => {
              const id = e.target.value as string;
              const video = data.find((d) => d.id === id);
              handleSet(id);
              setName(video ? video.title : '');
            }}
          >
            {data.map((v) => (
              <MenuItem disabled={checkIfAlreadyAdded(v.id)} key={v.id} value={v.id}>
                {v.title} {checkIfAlreadyAdded(v.id) && '(Hozzáadva)'}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
    </Grid>
  );
};

export default VideoSelect;
