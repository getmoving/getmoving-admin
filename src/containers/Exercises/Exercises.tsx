import React from 'react';
import { Grid } from '@material-ui/core';

import List from './components/ExercisesList';
import Form from './components/ExercisesForm';
import useExercises from './useExercises';

const Exercises: React.FC = () => {
  const {
    handleClick, dispatch, isLoading, videoId, categoryIds, countingType, count, equipmentIds, data,
  } = useExercises();

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} md={6} lg={6}>
        <Form
          handleClick={handleClick}
          videoId={videoId}
          categoryIds={categoryIds}
          countingType={countingType}
          count={count}
          equipmentIds={equipmentIds}
          dispatch={dispatch}
          isLoading={isLoading}
          data={data}
        />
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <List data={data} />
      </Grid>
    </Grid>
  );
};

export default Exercises;
