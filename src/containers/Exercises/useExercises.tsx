import React, { useReducer } from 'react';
import axios from 'axios';
import useAxios from 'axios-hooks';

import { API_ROOT } from '../../utils/constants';
import { exercisesInitialState, exercisesReducer, ExercisesProps } from './Exercises.reducer';

const useExercises = () => {
  const [state, dispatch] = useReducer(exercisesReducer, exercisesInitialState);
  const [{ data = [] }, refetch] = useAxios<ExercisesProps[]>(`${API_ROOT}/admin/get-exercises`);

  const [isLoading, setLoading] = React.useState(false);
  const { videoId, categoryIds, countingType, count, equipmentIds } = state;

  const handleClick = async () => {
    if (!videoId || !categoryIds.length || !countingType || !count) return;
    setLoading(true);
    try {
      await axios.post(`${API_ROOT}/admin/upload-exercise`, state);
      await refetch();
      dispatch(exercisesInitialState);
      // eslint-disable-next-line no-alert
      alert('Gyakorlat létrehozva!');
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(`Hiba: ${error.message}`);
    } finally {
      setLoading(false);
    }
  };

  return {
    handleClick,
    dispatch,
    isLoading,
    videoId,
    categoryIds,
    countingType,
    count,
    equipmentIds,
    data,
  };
};

export default useExercises;
