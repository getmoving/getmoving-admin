import React, { useReducer } from 'react';
import axios from 'axios';
import useAxios from 'axios-hooks';

import { storage } from '../../utils/firebase';
import { API_ROOT } from '../../utils/constants';
import uuid from '../../utils/uuid';
import resizeFile from '../../utils/resizeFile';
import { recipesReducer, recipesInitialState, RecipesProps } from './Recipes.reducer';
import { RecipeCategory } from './Recipes.types';

const useRecipes = () => {
  const [state, dispatch] = useReducer(recipesReducer, recipesInitialState);
  const [{ data = [] }, recipesRefetch] = useAxios<RecipesProps[]>(`${API_ROOT}/admin/get-recipes`);
  const [{ data: categories = [] }, categoryRefetch] = useAxios<RecipeCategory[]>(`${API_ROOT}/admin/get-recipe-categories`);
  const [isLoading, setLoading] = React.useState(false);
  const [category, setCategory] = React.useState('');
  const [numberOfIngedientBlocks, setBlocks] = React.useState(1);

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { imageFile, ...rest } = state;

  const handleClick = async () => {
    if (!state.imageFile) return;
    setLoading(true);
    const thumbnail = await resizeFile(state.imageFile);

    const uploadedFile = await storage.ref().child(`recipes/${uuid()}`).put(state.imageFile);
    const uploadedThumbnail = await storage.ref().child(`recipes/${uuid()}`).put(thumbnail);
    const imageUrl = await uploadedFile.ref.getDownloadURL();
    const thumbnailUrl = await uploadedThumbnail.ref.getDownloadURL();

    if (!imageUrl || !thumbnailUrl) return;
    try {
      await axios.post(`${API_ROOT}/admin/upload-recipe`, { ...rest, imageUrl, thumbnailUrl });
      await recipesRefetch();
      setBlocks(1);
      dispatch(recipesInitialState);
      // eslint-disable-next-line no-alert
      alert('Recept hozzáadva!');
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(`Hiba: ${error.message}`);
    } finally {
      setLoading(false);
    }
  };

  const categoryUpload = async () => {
    if (!category) return;
    setLoading(true);
    try {
      await axios.post(`${API_ROOT}/admin/upload-recipe-category`, { name: category });
      await categoryRefetch();
      setCategory('');
      // eslint-disable-next-line no-alert
      alert('Kategória hozzáadva!');
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(`Hiba: ${error.message}`);
    } finally {
      setLoading(false);
    }
  };

  return {
    dispatch,
    state,
    handleClick,
    isLoading,
    data,
    categoryRefetch,
    categories,
    category,
    setCategory,
    categoryUpload,
    numberOfIngedientBlocks,
    setBlocks,
  };
};

export default useRecipes;
