import React from 'react';
import { Grid, Box } from '@material-ui/core';

import useRecipes from './useRecipes';
import RecipeForm from './components/RecipeForm';
import RecipesList from './components/RecipesList';
import CategorySelect from './components/CategorySelect';
import CategoriesList from './components/CategoriesList';

const Recipes: React.FC = () => {
  const {
    dispatch, state, handleClick, isLoading, data, categories, categoryUpload, category,
    setCategory, numberOfIngedientBlocks, setBlocks,
  } = useRecipes();

  return (
    <Box>
      <Grid container spacing={4}>
        <Grid item xs={12} md={8}>
          <RecipeForm
            state={state}
            dispatch={dispatch}
            handleClick={handleClick}
            isLoading={isLoading}
            categories={categories}
            numberOfIngedientBlocks={numberOfIngedientBlocks}
            setBlocks={setBlocks}
          />
        </Grid>
        <Grid item xs={12} md={4}>
          <CategorySelect
            handleClick={categoryUpload}
            category={category}
            setCategory={setCategory}
            isLoading={isLoading}
          />
          <Box m={2} />
          <CategoriesList data={categories} />
        </Grid>
      </Grid>
      <Box m={4} />
      <Grid item xs={12} md={12} lg={12}>
        <RecipesList data={data} />
      </Grid>
    </Box>
  );
};

export default Recipes;
