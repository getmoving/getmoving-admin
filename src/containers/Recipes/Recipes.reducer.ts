import { RECIPE_LEVELS, IngredientsType } from './Recipes.types';

export const recipesInitialState = {
  title: '',
  level: 1 as RECIPE_LEVELS,
  duration: '' as unknown as number,
  details: '',
  body: '',
  block: [] as IngredientsType[],
  imageFile: null as (File | null),
  categoryIds: [] as string[],
};

export type RecipesState = typeof recipesInitialState;
type Action = Partial<RecipesState>;

interface Props extends RecipesState { id: string, imageUrl: string }
export type RecipesProps = Omit<Props, 'imageFile'>

export const recipesReducer = (s: RecipesState, a: Action) => ({ ...s, ...a });
