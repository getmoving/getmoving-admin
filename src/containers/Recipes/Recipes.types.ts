export enum RECIPE_LEVELS {
  EASY = 1,
  MEDIUM = 2,
  HARD = 3,
}

export interface IngredientsType {
    title: string;
    ingredients: string[],
}

export interface RecipeCategory {
  id: string;
  name: string;
}
