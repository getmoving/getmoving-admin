import { Grid, TextField, Typography } from '@material-ui/core';
import React from 'react';

interface Props {
  val: number;
  handleSet: React.Dispatch<number>;
}

const DurationInput: React.FC<Props> = ({ val, handleSet }) => (
  <Grid container alignItems="center">
    <Grid item xs={2} md={2} lg={2}>
      <Typography>Hossz</Typography>
    </Grid>
    <Grid item xs={10} md={10} lg={10}>
      <TextField
        variant="outlined"
        type="number"
        style={{ width: '100%' }}
        id="duration"
        value={val}
        onChange={(e) => handleSet(e.target.value ? Number(e.target.value) : '' as unknown as number)}
        InputProps={{
          endAdornment: 'perc',
        }}
      />
    </Grid>
  </Grid>
);

export default DurationInput;
