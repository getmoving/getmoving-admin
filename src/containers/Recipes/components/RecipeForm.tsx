import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent, CardActions, Button, CircularProgress, Typography, Box } from '@material-ui/core';

import DropZone from '../../../components/DropZone';
import NameInput from './NameInput';
import LevelSelect from './LevelSelect';
import { RecipesState } from '../Recipes.reducer';
import DurationInput from './DurationInput';
import DetailsInput from './DetailsInput';
import Ingredients from './Ingredients';
import BodyInput from './BodyInput';
import { RecipeCategory } from '../Recipes.types';
import CategoryInput from './CategoryInput';

interface Props {
  isLoading: boolean;
  handleClick: () => void;
  state: RecipesState;
  dispatch: React.Dispatch<Partial<RecipesState>>
  categories: RecipeCategory[];
  numberOfIngedientBlocks: number;
  setBlocks: (v: number) => void;
}

const RecipeForm: React.FC<Props> = ({
  handleClick, state, dispatch, isLoading, categories, numberOfIngedientBlocks, setBlocks,
}) => (
  <Card>
    <CardHeader title="Recept feltöltése" component="h3" disableTypography />
    <Divider />
    <CardContent>
      <form>
        <Grid container spacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <NameInput
              name={state.title}
              handleSet={(val) => dispatch({ title: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <LevelSelect
              val={state.level}
              handleSet={(val) => dispatch({ level: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <CategoryInput
              data={categories}
              val={state.categoryIds}
              handleSet={(v) => dispatch({ categoryIds: v })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <DurationInput
              val={state.duration}
              handleSet={(val) => dispatch({ duration: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <DetailsInput
              val={state.details}
              handleSet={(val) => dispatch({ details: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <BodyInput
              val={state.body}
              handleSet={(val) => dispatch({ body: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <Box my={4}>
              <Typography variant="h5">Hozzávalók</Typography>
              <Grid container>
                <Grid item xs={8} md={8} lg={8}>
                  <Typography>Csoport hozzadása/törlése</Typography>
                </Grid>
                <Grid item xs={4} md={4} lg={4}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      setBlocks(numberOfIngedientBlocks + 1);
                      const blockCopy = [...state.block];
                      blockCopy.push({ title: '', ingredients: [] });
                      dispatch({ block: blockCopy });
                    }}
                  >
                    +
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={numberOfIngedientBlocks <= 1}
                    onClick={() => {
                      setBlocks(numberOfIngedientBlocks - 1);
                      const blockCopy = [...state.block];
                      blockCopy.pop();
                      dispatch({ block: blockCopy });
                    }}
                  >
                    -
                  </Button>
                </Grid>
              </Grid>
              {Array(numberOfIngedientBlocks).fill('').map((_, i) => (
                <Ingredients
                // eslint-disable-next-line react/no-array-index-key
                  key={i}
                  val={state.block[i]}
                  handleSet={(v) => {
                    const blockCopy = [...state.block];
                    blockCopy[i] = v;
                    dispatch({ block: blockCopy });
                  }}
                />
              ))}
            </Box>
          </Grid>
          <Grid item xs={2} md={2} lg={2}>
            <Typography>Kép</Typography>
          </Grid>
          <Grid item xs={10} md={10} lg={10}>
            <DropZone file={state.imageFile} setFile={(f) => dispatch({ imageFile: f })} />
          </Grid>
        </Grid>
      </form>
    </CardContent>
    <Divider />
    <CardActions>
      <Button
        endIcon={isLoading && <CircularProgress size={16} />}
        disabled={isLoading || !state.block || !state.block.length || !state.body
            || !state.details || !state.duration || !state.imageFile || !state.level
            || !state.title || !state.categoryIds.length}
        variant="contained"
        type="submit"
        color="primary"
        onClick={handleClick}
      >
          Feltöltés
      </Button>
    </CardActions>
  </Card>
);

export default RecipeForm;
