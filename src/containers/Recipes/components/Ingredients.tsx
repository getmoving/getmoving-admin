import { Grid, Icon, InputAdornment, TextField, Tooltip, Typography } from '@material-ui/core';
import React from 'react';
import InfoIcon from '@material-ui/icons/Info';
import CreatableSelect from 'react-select/creatable';
import makeAnimated from 'react-select/animated';
import { SelectOptions } from 'src/containers/Groups/Groups.types';
import { IngredientsType } from '../Recipes.types';

interface Props {
  val: IngredientsType;
  handleSet: React.Dispatch<IngredientsType>;
}
const animatedComponents = makeAnimated();
const Ingredients: React.FC<Props> = ({ val = { title: '', ingredients: [] }, handleSet }) => (
  <Grid container alignItems="center">
    <Grid item xs={12} md={12} lg={12}>
      <Typography>Csoport neve</Typography>
    </Grid>
    <Grid item xs={12} md={12} lg={12}>
      <TextField
        variant="outlined"
        style={{ width: '100%' }}
        id="title"
        value={val.title}
        onChange={(e) => handleSet({ title: e.target.value, ingredients: val.ingredients })}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <Tooltip title="Pl. Lazac és köret">
                <Icon color="primary">
                  <InfoIcon />
                </Icon>
              </Tooltip>
            </InputAdornment>
          ),
        }}
      />
    </Grid>
    <Grid item xs={12} md={12} lg={12}>
      <Typography>Hozzávalók</Typography>
      <CreatableSelect
        components={animatedComponents}
        isMulti
        value={val.ingredients.map((i) => ({ label: i, value: i }))}
        onChange={(v) => v && handleSet({ title: val.title,
          ingredients: (v as SelectOptions[]).map((v) => v.label) })}
      />
    </Grid>
  </Grid>
);

export default Ingredients;
