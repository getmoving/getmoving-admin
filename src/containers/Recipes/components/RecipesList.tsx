import React from 'react';
import { Table, TableBody, TableRow, TableCell, TableHead, Divider, CardHeader, Card, CardContent } from '@material-ui/core';
import { RecipesProps } from '../Recipes.reducer';

const RecipesList: React.FC<{ data: RecipesProps[] }> = ({ data }) => {
  const renderBody = () => data.map((c) => (
    <TableRow key={c.id}>
      <TableCell>{c.title}</TableCell>
      <TableCell>{c.level}</TableCell>
      <TableCell>{c.duration}</TableCell>
      <TableCell>
        <img
          alt="recipe"
          src={c.imageUrl}
          style={{ maxWidth: 200, height: 100, objectFit: 'contain' }}
        />
      </TableCell>
    </TableRow>
  ));

  return (
    <Card>
      <CardHeader title="Recept lista" component="h3" disableTypography />
      <Divider />
      <CardContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Név</TableCell>
              <TableCell>Nehézség</TableCell>
              <TableCell>Időtartam</TableCell>
              <TableCell>Kép</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderBody()}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};

export default RecipesList;
