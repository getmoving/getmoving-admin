import React from 'react';
import { Grid, Select, FormControl, InputLabel, MenuItem } from '@material-ui/core';
import { RECIPE_LEVELS } from '../Recipes.types';

interface Props {
  val: RECIPE_LEVELS;
  handleSet: React.Dispatch<RECIPE_LEVELS>;
}

const LevelSelect: React.FC<Props> = ({ val, handleSet }) => (
  <Grid container alignItems="center">
    <Grid item xs={12} md={12} lg={12}>
      <FormControl style={{ width: '100%' }}>
        <InputLabel shrink>Válassz nehézséget</InputLabel>
        <Select
          value={val}
          onChange={(e) => handleSet(e.target.value as RECIPE_LEVELS)}
        >
          <MenuItem value={RECIPE_LEVELS.EASY}>Könnyű</MenuItem>
          <MenuItem value={RECIPE_LEVELS.MEDIUM}>Közepes</MenuItem>
          <MenuItem value={RECIPE_LEVELS.HARD}>Nehéz</MenuItem>
        </Select>
      </FormControl>
    </Grid>
  </Grid>
);

export default LevelSelect;
