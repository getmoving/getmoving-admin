import React from 'react';
import { Table, TableBody, TableRow, TableCell, TableHead, Divider, CardHeader, Card, CardContent } from '@material-ui/core';
import { RecipeCategory } from '../Recipes.types';

const CategoriesList: React.FC<{ data: RecipeCategory[] }> = ({ data }) => {
  const renderBody = () => data.map((c) => (
    <TableRow key={c.id}>
      <TableCell>{c.name}</TableCell>
    </TableRow>
  ));

  return (
    <Card>
      <CardHeader title="Kategória lista" component="h3" disableTypography />
      <Divider />
      <CardContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Név</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderBody()}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};

export default CategoriesList;
