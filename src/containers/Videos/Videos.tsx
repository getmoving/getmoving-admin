import React from 'react';
import { Grid, TextField } from '@material-ui/core';

import { VideoMetaDataCard, VideosList } from './components';
import useVideos from './useVideos';

const Videos: React.FC = () => {
  const {
    handleClick, id, url, title, description, duration, dispatch, data, isLoading, search,
    setSearch,
  } = useVideos();

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} md={6} lg={6}>
        <VideoMetaDataCard
          id={id}
          handleClick={handleClick}
          title={title}
          description={description}
          dispatch={dispatch}
          isLoading={isLoading}
          url={url}
          duration={duration}
        />
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <TextField
          variant="outlined"
          placeholder="Keresés"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <VideosList searchText={search} data={data} dispatch={dispatch} />
      </Grid>
    </Grid>
  );
};

export default Videos;
