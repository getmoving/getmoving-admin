import React from 'react';
import { Table, TableBody, TableRow, TableCell, TableHead, Divider, CardHeader, Card, CardContent, IconButton } from '@material-ui/core';
import { OpenInNew, Edit } from '@material-ui/icons';
import { VideosProps } from '../Videos.types';

interface Props {
  dispatch: React.Dispatch<Partial<{
    title: string;
    description: string;
    duration: number;
    url: string;
  }>>;
  data: VideosProps[];
  searchText: string;
}

const VideosList: React.FC<Props> = ({ data, dispatch, searchText }) => React.useMemo(() => {
  const filteredData = data.filter((d) => (d.title + d.description)
    .toLocaleLowerCase().includes(searchText.toLocaleLowerCase()));

  const renderBody = () => filteredData.map((e) => (
    <TableRow key={e.url + e.title + e.duration}>
      <TableCell>{e.title}</TableCell>
      <TableCell>{e.description}</TableCell>
      <TableCell>{e.duration}</TableCell>
      <TableCell style={{ minWidth: 100 }}>{new Date(e.date).toLocaleDateString()}</TableCell>
      <TableCell>
        <IconButton onClick={() => window.open(e.url, '__blank')}>
          <OpenInNew />
        </IconButton>
      </TableCell>
      <TableCell>
        <IconButton onClick={() => {
          dispatch(e);
          window.scrollTo({ top: 0, behavior: 'smooth' });
        }}
        >
          <Edit />
        </IconButton>
      </TableCell>
    </TableRow>
  ));

  return (
    <Card>
      <CardHeader title="Video lista" component="h3" disableTypography />
      <Divider />
      <CardContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Név</TableCell>
              <TableCell>Leírás</TableCell>
              <TableCell>Hossz</TableCell>
              <TableCell>Dátum</TableCell>
              <TableCell>URL</TableCell>
              <TableCell>Szerkesztés</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderBody()}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
}, [data, searchText]);

export default VideosList;
