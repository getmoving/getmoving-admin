import { Grid, Icon, InputAdornment, TextField, Tooltip, Typography } from '@material-ui/core';
import React from 'react';
import InfoIcon from '@material-ui/icons/Info';

interface Props {
  title: string;
  handleSet: React.Dispatch<string>;
}

const TitleInput: React.FC<Props> = ({ title, handleSet }) => (
  <Grid container alignItems="center">
    <Grid item xs={2} md={2} lg={2}>
      <Typography>Cím</Typography>
    </Grid>
    <Grid item xs={10} md={10} lg={10}>
      <TextField
        variant="outlined"
        style={{ width: '100%' }}
        id="title"
        value={title}
        onChange={(e) => handleSet(e.target.value)}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <Tooltip title="Lehetőleg egyezzen a youtube-on megadott címmel. (Milyen cím? A GECIM.)">
                <Icon color="primary">
                  <InfoIcon />
                </Icon>
              </Tooltip>
            </InputAdornment>
          ),
        }}
      />
    </Grid>
  </Grid>
);

export default TitleInput;
