import { Grid, Icon, InputAdornment, TextField, Tooltip, Typography } from '@material-ui/core';
import React from 'react';
import InfoIcon from '@material-ui/icons/Info';

interface Props {
  duration: number;
  handleSet: React.Dispatch<number>;
}

const DurationInput: React.FC<Props> = ({ duration, handleSet }) => (
  <Grid container alignItems="center">
    <Grid item xs={2} md={2} lg={2}>
      <Typography>Hossz</Typography>
    </Grid>
    <Grid item xs={10} md={10} lg={10}>
      <TextField
        variant="outlined"
        type="number"
        style={{ width: '100%' }}
        id="duration"
        value={duration}
        onChange={(e) => handleSet(Number(e.target.value))}
        InputProps={{
          endAdornment: (
            <>
              másodperc
              <InputAdornment position="end">
                <Tooltip title="Ha a videó pl. 3:21, akkor 3*60 + 21 = 201 másodperc a hossza">
                  <Icon color="primary">
                    <InfoIcon />
                  </Icon>
                </Tooltip>
              </InputAdornment>
            </>
          ),
        }}
      />
    </Grid>
  </Grid>
);

export default DurationInput;
