import { Grid, Icon, InputAdornment, TextField, Tooltip, Typography } from '@material-ui/core';
import React from 'react';
import InfoIcon from '@material-ui/icons/Info';

interface Props {
  description: string;
  handleSet: React.Dispatch<string>;
}

const DescriptionInput: React.FC<Props> = ({ description, handleSet }) => (
  <Grid container alignItems="center">
    <Grid item xs={2} md={2} lg={2}>
      <Typography>Leírás</Typography>
    </Grid>
    <Grid item xs={10} md={10} lg={10}>
      <TextField
        variant="outlined"
        style={{ width: '100%' }}
        id="description"
        multiline
        rows="2"
        rowsMax={5}
        value={description}
        onChange={(e) => handleSet(e.target.value)}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <Tooltip title="Rövid, pár mondatos leírás a videóról">
                <Icon color="primary">
                  <InfoIcon />
                </Icon>
              </Tooltip>
            </InputAdornment>
          ),
        }}
      />
    </Grid>
  </Grid>
);

export default DescriptionInput;
