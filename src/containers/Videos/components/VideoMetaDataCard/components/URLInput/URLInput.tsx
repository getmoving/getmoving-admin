import React from 'react';
import { Grid, Typography, TextField } from '@material-ui/core';

interface Props {
  url: string;
  handleSet: React.Dispatch<string>;
}

const URLInput: React.FC<Props> = ({ url, handleSet }) => (
  <Grid container alignItems="center">
    <Grid item xs={2} md={2} lg={2}>
      <Typography>URL</Typography>
    </Grid>
    <Grid item xs={10} md={10} lg={10}>
      <TextField
        style={{ width: '100%' }}
        variant="outlined"
        id="url"
        value={url}
        onChange={(e) => handleSet(e.target.value)}
      />
    </Grid>
  </Grid>
);

export default URLInput;
