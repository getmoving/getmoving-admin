import React from 'react';
import {
  Grid,
  Card,
  CardHeader,
  Divider,
  CardContent,
  Button,
  CardActions,
  CircularProgress,
} from '@material-ui/core';

import { URLInput, TitleInput, DescriptionInput, DurationInput } from './components';
import { videosInitialState } from '../../Videos.reducer';

interface Props {
  handleClick: () => void;
  id: string;
  title: string;
  description: string;
  isLoading: boolean;
  duration: number;
  url: string;
  dispatch: React.Dispatch<Partial<{
    title: string;
    description: string;
    duration: number;
    url: string;
  }>>
}

const VideoMetaDataCard: React.FC<Props> = ({
  handleClick, id, url, title, description, duration, dispatch, isLoading,
}) => (
  <Card>
    <CardHeader title={`Video ${id ? 'szerkesztése' : 'feltöltése'}`} component="h3" disableTypography />
    <Divider />
    <CardContent>
      <form>
        <Grid container spacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <URLInput
              url={url}
              handleSet={(val) => dispatch({ url: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <TitleInput
              title={title}
              handleSet={(val) => dispatch({ title: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <DescriptionInput
              description={description}
              handleSet={(val) => dispatch({ description: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <DurationInput
              duration={duration}
              handleSet={(val) => dispatch({ duration: val })}
            />
          </Grid>
        </Grid>
      </form>
    </CardContent>
    <Divider />
    <CardActions>
      <Button
        variant="contained"
        type="submit"
        color="primary"
        endIcon={isLoading && <CircularProgress size={16} />}
        disabled={isLoading || !title || !description || !duration || !url}
        onClick={handleClick}
      >
        {id ? 'Módosítás' : 'Feltöltés'}
      </Button>
      {id && (
        <Button
          variant="contained"
          disabled={isLoading}
          onClick={() => dispatch(videosInitialState)}
        >
          Mégse
        </Button>
      )}
    </CardActions>
  </Card>
);

export default VideoMetaDataCard;
