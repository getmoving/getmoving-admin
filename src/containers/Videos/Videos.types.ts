export interface VideosProps {
  id: string;
  title: string;
  description: string
  duration: number;
  date: string;
  url: string;
}
