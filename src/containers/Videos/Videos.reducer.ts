export const videosInitialState = {
  id: '',
  url: '',
  title: '',
  description: '',
  duration: 0,
};

type State = typeof videosInitialState;
type Action = Partial<State>;

export const videosReducer = (s: State, a: Action) => ({ ...s, ...a });
