import { useReducer, useState } from 'react';
import axios from 'axios';
import useAxios from 'axios-hooks';

import { API_ROOT } from '../../utils/constants';
import { VideosProps } from './Videos.types';
import { videosReducer, videosInitialState } from './Videos.reducer';

export default function useVideos() {
  const [state, dispatch] = useReducer(videosReducer, videosInitialState);
  const [search, setSearch] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [{ data = [] }, refetch] = useAxios<VideosProps[]>(`${API_ROOT}/admin/get-videos`);
  const { id, url, title, description, duration } = state;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { id: unused, ...rest } = state;

  const handleClick = async () => {
    if (!url || !title || !description || !duration) return;
    setLoading(true);
    try {
      if (id) {
        await axios.put(`${API_ROOT}/admin/update-videos/${id}`, rest);
      } else {
        await axios.post(`${API_ROOT}/admin/upload-video`, rest);
      }
      await refetch();
      dispatch(videosInitialState);
      // eslint-disable-next-line no-alert
      alert(`Video ${id ? 'módosítva' : 'hozzáadva'}!`);
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(`Hiba: ${error.message}`);
    } finally {
      setLoading(false);
    }
  };

  return {
    handleClick,
    url,
    title,
    description,
    duration,
    dispatch,
    isLoading,
    data,
    id,
    search,
    setSearch,
  };
}
