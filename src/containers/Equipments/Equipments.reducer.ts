export const equipmentsInitialState = {
  name: '',
  file: null as (File | null),
};

type State = typeof equipmentsInitialState;
type Action = Partial<State>;

export const equipmentsReducer = (s: State, a: Action) => ({ ...s, ...a });
