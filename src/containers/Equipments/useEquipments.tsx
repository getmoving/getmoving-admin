import React, { useReducer } from 'react';
import axios from 'axios';
import useAxios from 'axios-hooks';

import { storage } from '../../utils/firebase';
import { API_ROOT } from '../../utils/constants';
import uuid from '../../utils/uuid';
import { EquipmentsProps } from './Equipments.types';
import { equipmentsReducer, equipmentsInitialState } from './Equipments.reducer';

const useEquipments = () => {
  const [state, dispatch] = useReducer(equipmentsReducer, equipmentsInitialState);
  const [{ data = [] }, refetch] = useAxios<EquipmentsProps[]>(`${API_ROOT}/admin/get-equipments`);
  const [isLoading, setLoading] = React.useState(false);
  const { name, file } = state;

  const handleClick = async () => {
    if (!file || !name) return;
    setLoading(true);
    const uploadedFile = await storage.ref().child(`equipments/${uuid()}`).put(file);
    const imageUrl = await uploadedFile.ref.getDownloadURL();
    if (!imageUrl) return;
    try {
      await axios.post(`${API_ROOT}/admin/upload-equipment`, { name, imageUrl });
      await refetch();
      dispatch(equipmentsInitialState);
      // eslint-disable-next-line no-alert
      alert('Felszerelés hozzáadva!');
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(`Hiba: ${error.message}`);
    } finally {
      setLoading(false);
    }
  };

  return { handleClick, name, file, dispatch, isLoading, data };
};

export default useEquipments;
