import React from 'react';
import { Table, TableBody, TableRow, TableCell, TableHead, Divider, CardHeader, Card, CardContent } from '@material-ui/core';
import { EquipmentsProps } from '../Equipments.types';

const EquipmentsList: React.FC<{ data: EquipmentsProps[] }> = ({ data }) => {
  const renderBody = () => data.map((e) => (
    <TableRow key={e.imageUrl}>
      <TableCell>{e.name}</TableCell>
      <TableCell>
        <img
          alt="equipment"
          src={e.imageUrl}
          style={{ maxWidth: 200, height: 100, objectFit: 'contain' }}
        />
      </TableCell>
    </TableRow>
  ));

  return (
    <Card>
      <CardHeader title="Felszerelés lista" component="h3" disableTypography />
      <Divider />
      <CardContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Név</TableCell>
              <TableCell>Kép</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderBody()}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};

export default EquipmentsList;
