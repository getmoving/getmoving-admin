import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent, CardActions, Button, CircularProgress, Typography } from '@material-ui/core';

import DropZone from '../../../components/DropZone';
import NameInput from './NameInput';

interface Props {
  handleClick: () => void;
  name: string;
  isLoading: boolean;
  file: File | null;
  dispatch: React.Dispatch<Partial<{
    name: string;
    file: File | null;
  }>>
}

const EquipmentForm: React.FC<Props> = ({
  handleClick, name, dispatch, isLoading, file,
}) => (
  <Card>
    <CardHeader title="Felszerelés feltöltése" component="h3" disableTypography />
    <Divider />
    <CardContent>
      <form>
        <Grid container spacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <NameInput
              name={name}
              handleSet={(val) => dispatch({ name: val })}
            />
          </Grid>
          <Grid item xs={2} md={2} lg={2}>
            <Typography>Kép</Typography>
          </Grid>
          <Grid item xs={10} md={10} lg={10}>
            <DropZone file={file} setFile={(f) => dispatch({ file: f })} />
          </Grid>
        </Grid>
      </form>
    </CardContent>
    <Divider />
    <CardActions>
      <Button
        endIcon={isLoading && <CircularProgress size={16} />}
        disabled={isLoading || !name || !file}
        variant="contained"
        type="submit"
        color="primary"
        onClick={handleClick}
      >
        Feltöltés
      </Button>
    </CardActions>
  </Card>
);

export default EquipmentForm;
