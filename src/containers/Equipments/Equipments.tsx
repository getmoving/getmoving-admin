import React from 'react';
import { Grid } from '@material-ui/core';

import List from './components/EquipmentsList';
import Form from './components/EquipmentForm';
import useEquipments from './useEquipments';

const Equipments: React.FC = () => {
  const { handleClick, name, dispatch, isLoading, data, file } = useEquipments();

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} md={6} lg={6}>
        <Form
          handleClick={handleClick}
          name={name}
          dispatch={dispatch}
          isLoading={isLoading}
          file={file}
        />
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <List data={data} />
      </Grid>
    </Grid>
  );
};

export default Equipments;
