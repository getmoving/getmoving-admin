export interface EquipmentsProps {
  id: string;
  name: string;
  imageUrl: string;
}
