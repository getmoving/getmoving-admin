import React from 'react';
import { Grid } from '@material-ui/core';

import CategoriesList from './components/CategoriesList';
import CategoryForm from './components/CategoryForm';
import useCategories from './useCategories';

const Categories: React.FC = () => {
  const { handleClick, name, description, dispatch, isLoading, data, file } = useCategories();

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} md={6} lg={6}>
        <CategoryForm
          handleClick={handleClick}
          name={name}
          description={description}
          dispatch={dispatch}
          isLoading={isLoading}
          file={file}
        />
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <CategoriesList data={data} />
      </Grid>
    </Grid>
  );
};

export default Categories;
