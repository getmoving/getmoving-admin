import React, { useReducer } from 'react';
import axios from 'axios';
import useAxios from 'axios-hooks';

import { storage } from '../../utils/firebase';
import { API_ROOT } from '../../utils/constants';
import uuid from '../../utils/uuid';
import { CategoriesProps } from './Categories.types';
import { categoriesReducer, categoriesInitialState } from './Categories.reducer';

const useCategories = () => {
  const [state, dispatch] = useReducer(categoriesReducer, categoriesInitialState);
  const [{ data = [] }, refetch] = useAxios<CategoriesProps[]>(`${API_ROOT}/admin/get-categories`);
  const [isLoading, setLoading] = React.useState(false);
  const { name, description, file } = state;

  const handleClick = async () => {
    if (!file || !name || !description) return;
    setLoading(true);
    const uploadedFile = await storage.ref().child(`categories/${uuid()}`).put(file);
    const imageUrl = await uploadedFile.ref.getDownloadURL();
    if (!imageUrl) return;
    try {
      await axios.post(`${API_ROOT}/admin/upload-category`, { name, description, imageUrl });
      await refetch();
      dispatch(categoriesInitialState);
      // eslint-disable-next-line no-alert
      alert('Kategória hozzáadva!');
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(`Hiba: ${error.message}`);
    } finally {
      setLoading(false);
    }
  };

  return { handleClick, name, file, description, dispatch, isLoading, data };
};

export default useCategories;
