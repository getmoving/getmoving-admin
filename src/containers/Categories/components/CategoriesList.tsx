import React from 'react';
import { Table, TableBody, TableRow, TableCell, TableHead, Divider, CardHeader, Card, CardContent } from '@material-ui/core';
import { CategoriesProps } from '../Categories.types';

const CategoriesList: React.FC<{ data: CategoriesProps[] }> = ({ data }) => {
  const renderBody = () => data.map((c) => (
    <TableRow key={c.imageUrl}>
      <TableCell>{c.name}</TableCell>
      <TableCell>{c.description}</TableCell>
      <TableCell>
        <img
          alt="category"
          src={c.imageUrl}
          style={{ maxWidth: 200, height: 100, objectFit: 'contain' }}
        />
      </TableCell>
    </TableRow>
  ));

  return (
    <Card>
      <CardHeader title="Kategória lista" component="h3" disableTypography />
      <Divider />
      <CardContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Név</TableCell>
              <TableCell>Leírás</TableCell>
              <TableCell>Kép</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderBody()}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};

export default CategoriesList;
