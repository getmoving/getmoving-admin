export const categoriesInitialState = {
  name: '',
  description: '',
  file: null as (File | null),
};

type State = typeof categoriesInitialState;
type Action = Partial<State>;

export const categoriesReducer = (s: State, a: Action) => ({ ...s, ...a });
