export interface CategoriesProps {
  id: string;
  name: string;
  description: string;
  imageUrl: string;
}
