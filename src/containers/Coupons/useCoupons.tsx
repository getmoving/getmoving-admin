/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useReducer } from 'react';
import axios from 'axios';
import useAxios from 'axios-hooks';

import { API_ROOT } from '../../utils/constants';
import { CouponsProps } from './Coupons.types';
import { couponsReducer, couponsInitialState } from './Coupons.reducer';

const useCoupons = () => {
  const [state, dispatch] = useReducer(couponsReducer, couponsInitialState);
  const [{ data = [] }, refetch] = useAxios<CouponsProps[]>(`${API_ROOT}/admin/get-coupons`);
  const [isLoading, setLoading] = React.useState(false);
  const { code, percent } = state;

  const upload = async () => {
    if (!code || !percent) return;
    setLoading(true);
    try {
      await axios.post(`${API_ROOT}/admin/upload-coupon`, { code, percent, isValid: true });
      await refetch();
      dispatch(couponsInitialState);
      // eslint-disable-next-line no-alert
      alert('Kupon hozzáadva!');
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(`Hiba: ${(error as any).message}`);
    } finally {
      setLoading(false);
    }
  };

  const toggleValidity = async (id: string, isValid: boolean) => {
    setLoading(true);
    try {
      await axios.put(`${API_ROOT}/admin/update-coupon/${id}`, { isValid });
      await refetch();
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(`Hiba: ${(error as any).message}`);
    } finally {
      setLoading(false);
    }
  };

  return { upload, toggleValidity, code, percent, dispatch, isLoading, data };
};

export default useCoupons;
