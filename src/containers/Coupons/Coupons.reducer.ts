export const couponsInitialState = {
  code: '',
  isValid: false,
  percent: '' as number | '',
};

type State = typeof couponsInitialState;
type Action = Partial<State>;

export const couponsReducer = (s: State, a: Action) => ({ ...s, ...a });
