import React from 'react';
import { Grid } from '@material-ui/core';

import List from './components/CouponsList';
import Form from './components/CouponForm';
import useCoupons from './useCoupons';

const Coupons: React.FC = () => {
  const { upload, toggleValidity, code, percent, dispatch, isLoading, data } = useCoupons();

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} md={6} lg={6}>
        <Form
          upload={upload}
          code={code}
          dispatch={dispatch}
          isLoading={isLoading}
          percent={percent}
        />
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <List data={data} toggleValidity={toggleValidity} />
      </Grid>
    </Grid>
  );
};

export default Coupons;
