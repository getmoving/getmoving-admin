import React from 'react';
import { Table, TableBody, TableRow, TableCell, TableHead, Divider, CardHeader, Card, CardContent, Switch } from '@material-ui/core';
import { CouponsProps } from '../Coupons.types';

const CouponsList: React.FC<{
  data: CouponsProps[],
  toggleValidity: (id: string, isValid: boolean) => void,
}> = ({ data, toggleValidity }) => {
  const renderBody = () => data.map((e) => (
    <TableRow key={e.id}>
      <TableCell>{e.code}</TableCell>
      <TableCell>{e.percent}</TableCell>
      <TableCell>
        <Switch
          checked={e.isValid}
          onChange={(v) => toggleValidity(e.id, v.target.checked)}
        />
      </TableCell>
    </TableRow>
  ));

  return (
    <Card>
      <CardHeader title="Kupon lista" component="h3" disableTypography />
      <Divider />
      <CardContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Kód</TableCell>
              <TableCell>Kedvezmény (%)</TableCell>
              <TableCell>Aktív</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderBody()}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};

export default CouponsList;
