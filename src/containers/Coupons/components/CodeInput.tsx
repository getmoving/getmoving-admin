import { Grid, TextField, Typography } from '@material-ui/core';
import React from 'react';

interface Props {
  code: string;
  handleSet: React.Dispatch<string>;
}

const CodeInput: React.FC<Props> = ({ code, handleSet }) => (
  <Grid container alignItems="center">
    <Grid item xs={2} md={2} lg={2}>
      <Typography>Kuponkód</Typography>
    </Grid>
    <Grid item xs={10} md={10} lg={10}>
      <TextField
        variant="outlined"
        style={{ width: '100%' }}
        id="code"
        value={code}
        onChange={(e) => handleSet(e.target.value)}
      />
    </Grid>
  </Grid>
);

export default CodeInput;
