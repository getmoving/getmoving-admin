import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent, CardActions, Button, CircularProgress } from '@material-ui/core';
import CodeInput from './CodeInput';
import PercentInput from './PercentInput';

interface Props {
  upload: () => void;
  code: string;
  percent: number | '';
  isLoading: boolean;
  dispatch: React.Dispatch<Partial<{
    code: string;
    percent: number | '';
  }>>
}

const CouponForm: React.FC<Props> = ({ upload, code, percent, dispatch, isLoading }) => (
  <Card>
    <CardHeader title="Kuponkód hozzáadása" component="h3" disableTypography />
    <Divider />
    <CardContent>
      <form>
        <Grid container spacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <CodeInput
              code={code}
              handleSet={(val) => dispatch({ code: val })}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <PercentInput
              percent={percent}
              handleSet={(val) => dispatch({ percent: val })}
            />
          </Grid>
        </Grid>
      </form>
    </CardContent>
    <Divider />
    <CardActions>
      <Button
        endIcon={isLoading && <CircularProgress size={16} />}
        disabled={isLoading || !code || !percent}
        variant="contained"
        type="submit"
        color="primary"
        onClick={upload}
      >
        Feltöltés
      </Button>
    </CardActions>
  </Card>
);

export default CouponForm;
