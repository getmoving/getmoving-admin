import { Grid, Icon, InputAdornment, TextField, Tooltip, Typography } from '@material-ui/core';
import React from 'react';
import InfoIcon from '@material-ui/icons/Info';

interface Props {
  percent: number | '';
  handleSet: React.Dispatch<number | ''>;
}

const percentInput: React.FC<Props> = ({ percent, handleSet }) => (
  <Grid container alignItems="center">
    <Grid item xs={2} md={2} lg={2}>
      <Typography>Akció</Typography>
    </Grid>
    <Grid item xs={10} md={10} lg={10}>
      <TextField
        variant="outlined"
        style={{ width: '100%' }}
        id="percent"
        type="number"
        value={percent}
        onChange={(e) => handleSet(e.target.value ? Number(e.target.value) : '')}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <Tooltip title="A kedvezmény % -ban értendő">
                <Icon color="primary">
                  <InfoIcon />
                </Icon>
              </Tooltip>
            </InputAdornment>
          ),
        }}
      />
    </Grid>
  </Grid>
);

export default percentInput;
