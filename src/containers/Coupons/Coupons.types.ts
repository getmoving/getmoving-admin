export interface CouponsProps {
  id: string;
  code: string;
  isValid: boolean;
  percent: number | '';
}
