import React from 'react';
import { Grid, Card, CardHeader, Divider, CardContent, CardActions, Button, CircularProgress } from '@material-ui/core';

import NameInput from './NameInput';
import { LevelsType } from '../Groups.types';
import ExercisesSelect from './ExercisesSelect';

interface Props {
  handleClick: () => void;
  groupType: string;
  isLoading: boolean;
  levels: LevelsType[];
  dispatch: React.Dispatch<Partial<{
    groupType: string;
    levels: LevelsType[];
  }>>
}

const GroupsForm: React.FC<Props> = ({ handleClick, groupType, dispatch, isLoading, levels }) => (
  <Card style={{ overflow: 'initial' }}>
    <CardHeader title="Csoport feltöltése" component="h3" disableTypography />
    <Divider />
    <CardContent>
      <form>
        <Grid container spacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <NameInput
              v={groupType}
              handleSet={(val) => dispatch({ groupType: val })}
            />
          </Grid>
          {levels.map(({ level, name, exercises }) => (
            <Grid key={name} item xs={12} md={12} lg={12}>
              <ExercisesSelect
                title={name}
                val={exercises}
                handleSet={(v) => {
                  const levelsCopy = [...levels];
                  levelsCopy.splice(level, 1, { exercises: v, level, name });
                  dispatch({ levels: levelsCopy });
                }}
              />
            </Grid>
          ))}
        </Grid>
      </form>
    </CardContent>
    <Divider />
    <CardActions>
      <Button
        endIcon={isLoading && <CircularProgress size={16} />}
        disabled={isLoading || !groupType}
        variant="contained"
        type="submit"
        color="primary"
        onClick={handleClick}
      >
        Mentés
      </Button>
    </CardActions>
  </Card>
);

export default GroupsForm;
