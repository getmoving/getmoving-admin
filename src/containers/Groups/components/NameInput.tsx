import React from 'react';
import { Grid, Select, MenuItem, FormControl, InputLabel } from '@material-ui/core';
import useAxios from 'axios-hooks';
import { API_ROOT } from '../../../utils/constants';

interface Props {
  v: string;
  handleSet: React.Dispatch<string>;
}

const NameInput: React.FC<Props> = ({ v, handleSet }) => {
  const [{ data = [] }] = useAxios<{ id: string, name: string }[]>(`${API_ROOT}/admin/get-group-types`);

  return (
    <Grid container alignItems="center">
      <Grid item xs={12} md={12} lg={12}>
        <FormControl style={{ width: '100%' }}>
          <InputLabel shrink>Név</InputLabel>
          <Select
            value={v}
            onChange={(e) => handleSet(e.target.value as string)}
          >
            {data.map(({ id, name }) => (
              <MenuItem key={id} value={id}>{name}</MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
    </Grid>
  );
};

export default NameInput;
