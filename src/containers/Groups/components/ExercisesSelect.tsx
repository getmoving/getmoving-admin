import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import useAxios from 'axios-hooks';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

import { ExercisesProps } from 'src/containers/Exercises/Exercises.reducer';
import { SelectOptions } from '../Groups.types';
import { API_ROOT } from '../../../utils/constants';

interface ExercisesPropsWithId extends ExercisesProps {
  id: string;
}

interface Props {
  title: string;
  val: SelectOptions[];
  handleSet: React.Dispatch<SelectOptions[]>;
}

const animatedComponents = makeAnimated();
const ExercisesSelect: React.FC<Props> = ({ title, val, handleSet }) => {
  const [{ data = [] }] = useAxios<ExercisesPropsWithId[]>(`${API_ROOT}/admin/get-exercises`);

  const options: SelectOptions[] = data.map((d) => ({ value: d.id, label: d.name }));

  return (
    <Grid container alignItems="center">
      <Grid item xs={12} md={12} lg={12}>
        <Typography>{title}</Typography>
        <Select
          closeMenuOnSelect={false}
          components={animatedComponents}
          value={val}
          onChange={(v) => v && handleSet(v as SelectOptions[])}
          isMulti
          options={options}
        />
      </Grid>
    </Grid>
  );
};

export default ExercisesSelect;
