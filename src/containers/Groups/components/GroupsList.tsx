import React from 'react';
import useAxios from 'axios-hooks';
import { Table, TableBody, TableRow, TableCell, TableHead, Divider, CardHeader, Card, CardContent } from '@material-ui/core';

import { GroupsProps } from '../Groups.types';
import { ExercisesProps } from '../../Exercises/Exercises.reducer';
import { API_ROOT } from '../../../utils/constants';

interface ExercisesPropsWithId extends ExercisesProps {
  id: string;
}

const GroupsList: React.FC<{ data: GroupsProps[] }> = ({ data }) => {
  const [{ data: exercises = [] }] = useAxios<ExercisesPropsWithId[]>(`${API_ROOT}/admin/get-exercises`);
  const [{ data: groupTypes = [] }] = useAxios<{ id: string, name: string }[]>(`${API_ROOT}/admin/get-group-types`);

  const renderBody = () => data.map((e) => {
    const groupType = groupTypes.find((g) => g.id === e.groupType);

    return (
      <TableRow key={e.id}>
        <TableCell>{groupType && groupType.name}</TableCell>
        <TableCell>{e.levels.map((l) => `${l.name}: ${l.exercises.map((e) => {
          const exercise = exercises.find((ex) => ex.id === e.value);
          if (!exercise) return null;
          return exercise.name;
        })}; `)}
        </TableCell>
      </TableRow>
    );
  });

  return (
    <Card>
      <CardHeader title="Csoport lista" component="h3" disableTypography />
      <Divider />
      <CardContent>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Név</TableCell>
              <TableCell>Gyakorlatok</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderBody()}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};

export default GroupsList;
