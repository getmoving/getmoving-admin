import React, { useReducer, useEffect } from 'react';
import axios from 'axios';
import useAxios from 'axios-hooks';

import { API_ROOT } from '../../utils/constants';
import { GroupsProps } from './Groups.types';
import { groupsReducer, groupsInitialState } from './Groups.reducer';

const useGroups = () => {
  const [state, dispatch] = useReducer(groupsReducer, groupsInitialState);
  const [{ data = [] }, refetch] = useAxios<GroupsProps[]>(`${API_ROOT}/admin/get-groups`);
  const [isLoading, setLoading] = React.useState(false);
  const { groupType, levels } = state;
  const group = data.find((d) => state.groupType === d.groupType);

  useEffect(() => {
    if (!state.groupType || !group) return;
    dispatch({ levels: group.levels });
  }, [state.groupType]);

  const handleClick = async () => {
    if (!levels.length || !groupType) return;
    setLoading(true);
    try {
      if (group) {
        await axios.put(`${API_ROOT}/admin/update-group/${group.id}`, state);
      } else {
        await axios.post(`${API_ROOT}/admin/upload-group`, state);
      }
      await refetch();
      dispatch(groupsInitialState);
      // eslint-disable-next-line no-alert
      alert(group ? 'Csoport módosítva!' : 'Csoport hozzáadva!');
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(`Hiba: ${error.message}`);
    } finally {
      setLoading(false);
    }
  };

  return { handleClick, groupType, levels, dispatch, isLoading, data };
};

export default useGroups;
