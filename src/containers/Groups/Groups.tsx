import React from 'react';
import { Grid } from '@material-ui/core';

import List from './components/GroupsList';
import Form from './components/GroupsForm';
import useGroups from './useGroups';

const Groups: React.FC = () => {
  const { handleClick, groupType, dispatch, isLoading, data, levels } = useGroups();

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} md={6} lg={6}>
        <Form
          handleClick={handleClick}
          groupType={groupType}
          levels={levels}
          dispatch={dispatch}
          isLoading={isLoading}
        />
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <List data={data} />
      </Grid>
    </Grid>
  );
};

export default Groups;
