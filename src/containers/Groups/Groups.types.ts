export interface SelectOptions {
  label: string;
  value: string;
}

export interface LevelsType {
  level: number;
  name: string;
  exercises: SelectOptions[]
}

export interface GroupsProps {
  id: string;
  groupType: string;
  levels: LevelsType[];
}
