import { LevelsType } from './Groups.types';

export const groupsInitialState = {
  groupType: '',
  levels: [
    { level: 0, name: 'Fitness 1', exercises: [] },
    { level: 1, name: 'Fitness 2', exercises: [] },
    { level: 2, name: 'Fitness 3', exercises: [] },
    { level: 3, name: 'Fitness 4', exercises: [] },
    { level: 4, name: 'Fitness 5', exercises: [] },
    { level: 5, name: 'Fitness 6', exercises: [] },
    { level: 6, name: 'Fitness 7', exercises: [] },
    { level: 7, name: 'Fitness 8', exercises: [] },
    { level: 8, name: 'Fitness 9', exercises: [] },
    { level: 9, name: 'Fitness 10', exercises: [] },
  ] as LevelsType[],
};

type State = typeof groupsInitialState;
type Action = Partial<State>;

export const groupsReducer = (s: State, a: Action) => ({ ...s, ...a });
