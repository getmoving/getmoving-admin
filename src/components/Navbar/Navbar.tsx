import React, { useState, useEffect } from 'react';
import { Paper, Tabs, Tab } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';

import styles from './Navbar.styles';

const useStyles = makeStyles(styles);

const Nav: React.FC = () => {
  const { root } = useStyles();
  const history = useHistory();
  const [value, setValue] = useState<string>('/');

  useEffect(() => {
    history.push(value);
  }, [value]);

  const handleChange = (event: React.ChangeEvent<unknown>, newValue: string) => {
    setValue(newValue);
  };

  return (
    <Paper className={root}>
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto"
      >
        <Tab label="Főoldal" value="/" />
        <Tab label="Videók" value="/videos" />
        <Tab label="Kategóriák" value="/categories" />
        <Tab label="Felszerelések" value="/equipments" />
        <Tab label="Gyakorlatok" value="/exercises" />
        <Tab label="Csoportok" value="/groups" />
        <Tab label="Receptek" value="/recipes" />
        <Tab label="Blog" value="/blogs" />
        <Tab label="Fizetések" value="/payments" />
        <Tab label="Felhasználók" value="/users" />
        <Tab label="Kuponok" value="/coupons" />
      </Tabs>
    </Paper>
  );
};

export default Nav;
