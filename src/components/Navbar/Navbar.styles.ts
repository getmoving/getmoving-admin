import { createStyles } from '@material-ui/core/styles';

const styles = () => createStyles({
  root: {
    background: '#ffffff',
    flexGrow: 1,
    borderRadius: 'unset',
    margin: 'unset',
    padding: 'unset',
  },
});

export default styles;
