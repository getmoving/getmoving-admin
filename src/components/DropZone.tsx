import React, { useEffect } from 'react';
import { useDropzone } from 'react-dropzone';
import { Box, Typography } from '@material-ui/core';

interface Props {
  setFile: (file: File | null) => void;
  file: File | null;
  square?: boolean;
}

const Dropzone: React.FC<Props> = ({ setFile, file, square }) => {
  const [preview, setPreview] = React.useState<string>();
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    onDrop: (acceptedFiles) => {
      setPreview(URL.createObjectURL(acceptedFiles[0]));
    },
  });
  useEffect(() => setFile(acceptedFiles[0]), [acceptedFiles]);

  return (
    <Box>
      <Box {...getRootProps({ className: 'dropzone' })}>
        <input {...getInputProps()} />
        <Box border="1px dashed grey" py={3} px={5} borderRadius={8} style={{ cursor: 'pointer' }}>
          <Typography>Drag n drop fájlfeltöltés</Typography>
        </Box>
      </Box>
      {file && (
        <Box>
          <Typography component="h4" style={{ fontWeight: 'bold' }}>Fájl név</Typography>
          <Typography>{file.path} - {file.size} bytes</Typography>
          <Box height={220} width={square ? 220 : 393}>
            <img
              style={{ height: '100%', width: '100%', objectFit: square ? 'contain' : 'cover' }}
              alt="preview"
              src={preview}
            />
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default Dropzone;
