module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    'airbnb',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:import/typescript',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    ENV_PRODUCTION: 'readonly',
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    createDefaultProgram: true, // temporary fix for parserOptions error
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2020,
    project: './tsconfig.json',
    sourceType: 'module',
  },
  plugins: ['react', '@typescript-eslint', 'import'],
  rules: {
    quotes: ['error', 'single'],
    indent: [2, 2],
    // eslint-disable-next-line no-dupe-keys
    indent: ['error', 2, { SwitchCase: 1 }],
    'comma-dangle': [2, 'always-multiline'],
    'lines-between-class-members': 0,
    'object-curly-newline': 0,
    'no-useless-constructor': 0,
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: [
          '**/*.test.*',
          '**/*.spec.*',
          'buildconfig/**',
          '.storybook/**',
          '**/*.stories.tsx',
        ],
      },
    ],
    'import/no-useless-path-segments': [
      'error',
      {
        noUselessIndex: true,
      },
    ],
    'import/extensions': [
      'error',
      'never',
      { json: 'always' },
    ],
    // eslint-disable-next-line no-useless-escape
    'import/no-unresolved': [2, { ignore: ['\.png$', '\.jpg$'] }],
    'react/jsx-filename-extension': [2, { extensions: ['.tsx', '.jsx'] }],
    'no-use-before-define': 0,
    'react/jsx-indent': 0,
    'react/display-name': 0,
    'no-shadow': 0,
    'react/require-default-props': 0,
    'react/jsx-one-expression-per-line': 0,
    'react/jsx-props-no-multi-spaces': 0, // Disabled because of bug https://github.com/yannickcr/eslint-plugin-react/issues/2181
    'react/prop-types': 0,
    'react/jsx-props-no-spreading': 0,
    '@typescript-eslint/indent': 0,
    '@typescript-eslint/explicit-function-return-type': 0,
    '@typescript-eslint/no-empty-interface': 1,
    '@typescript-eslint/no-empty-function': 0,
    '@typescript-eslint/no-useless-constructor': 2,
    '@typescript-eslint/explicit-module-boundary-types': 0,
    // 'no-restricted-syntax': [
    //   'error',
    //   {
    //     selector: 'ImportDeclaration[source.value=/\\.\\./]',
    //     message:
    //       'No parent imports (../) allowed, import package and root modules by name,
    // otherwise rewrite to not need to import from parent level',
    //   },
    //   {
    //     selector: 'ClassProperty[value=null][optional!=true]
    //  Decorator[expression.name="observable"]',
    //     message: 'Use the decorate function for observables initialized in the constructor',
    //   },
    // ],
  },
  settings: {
    react: {
      version: 'detect',
    },
    'import/resolver': {
      typescript: {},
    },
  },
};
