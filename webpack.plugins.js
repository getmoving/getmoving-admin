/* eslint-disable @typescript-eslint/no-var-requires */
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const { DefinePlugin } = require('webpack');

module.exports = [
  new ForkTsCheckerWebpackPlugin(),
  new TsconfigPathsPlugin({ configFile: './tsconfig.json' }),
  new DefinePlugin({
    ENV_PRODUCTION: JSON.stringify(process.env.ENV_PRODUCTION === 'true'),
  }),
];
